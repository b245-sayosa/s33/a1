// console.log("Hello")
fetch using get default
fetch("https://jsonplaceholder.typicode.com/todos")
	 .then(response => response.json())
	 .then(data => {
	    //map
	    let title = data.map(element => element.title);
	    console.log(title);
	  });


//retrieving single todo list
fetch("https://jsonplaceholder.typicode.com/todos/17")
  .then(response => response.json())
  .then(result => console.log(result))
  //printing title and result
  fetch("https://jsonplaceholder.typicode.com/todos/17")
  .then(response => response.json())
  .then(result => console.log(`${result.title}, ${result.completed}`))


//fetch request using POST

   fetch("https://jsonplaceholder.typicode.com/todos",{
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    
    //body - contains the data that will be added to the database
    body: JSON.stringify({
        title: "New todos",
        body: "Hi World!",
        userId: 11,
        id: 201
    })
  })
  .then(response => response.json())
  .then(result => console.log(result));

  //fetch using PUT

     fetch("https://jsonplaceholder.typicode.com/todos/18",{
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    
    //body - contains the data that will be added to the database
    body: JSON.stringify({
        title: "17 Feborit number",
        body: "Hilu Werld",
        completed: false,
        
    })
  })
  .then(response => response.json())
  .then(result => console.log(result));



  //updating the to do list item


     fetch("https://jsonplaceholder.typicode.com/todos/179",{
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    
    //body - contains the data that will be added to the database
    body: JSON.stringify({ 
            title: "omnis consequuntur cupiditate impedit itaque ipsam quo",
            description: "pls describe me",
            completed: true , 
            dateCompleted: "pls insert date",
            userId: 9
    })
  })
  .then(response => response.json())
  .then(result => console.log(result));

  //update using patch
  fetch("https://jsonplaceholder.typicode.com/todos/6",{
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    
    //body - contains the data that will be added to the database
    body: JSON.stringify({ 
            title: "Updated",   
    })
  })
  .then(response => response.json())
  .then(result => console.log(result));

    //update date

    fetch("https://jsonplaceholder.typicode.com/todos/6",{
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    
    //body - contains the data that will be added to the database
    body: JSON.stringify({ 
            completed: true,
            dateCompleted: "4 mins ago",   
    })
  })
  .then(response => response.json())
  .then(result => console.log(result));




  





